import React, { useEffect, useRef, useState } from "react";
import type { NextPage } from "next";
import { Button, Img, Stack, Text, useToast, useColorModeValue as mode } from "@chakra-ui/react";

import { PageLayout } from "src/components/Layout/Pages/PageLayout.component";
import { Section } from "src/components/Layout/Section/Section.component";
import { H1 } from "src/components/Text/Titles.component";
import { ImageUploader } from "./components/ImageUploader.component";
import { LoadModel } from "./components/LoadModel.component";

import * as tf from "@tensorflow/tfjs";

const Home: NextPage = () => {
  const toast = useToast();
  const canvasRef = useRef(null);

  const modelUrl =
    "https://firebasestorage.googleapis.com/v0/b/waraness.appspot.com/o/smiling-model.json?alt=media&token=18e67545-fb3d-4f59-b6b1-db532e2a09dd";
  const [model, setModel] = useState<any>();
  const [imageSrc, setImageSrc] = useState<string>();
  const [prediction, setPrediction] = useState<number>();

  const onBeginUpload = (files: File[]) => {
    const reader = new FileReader();
    reader.readAsDataURL(files[0]);
    reader.onload = () => {
      setImageSrc(reader.result as string);
    };
    reader.onerror = (error) => {
      toast({
        title: "An error ocurred while uploading your file...",
        status: "error",
        isClosable: true,
      });
    };
  };

  const drawImageOnCanvas = (
    image: CanvasImageSource | OffscreenCanvas,
    canvas: any,
    ctx: CanvasRenderingContext2D | null,
  ) => {
    canvas.width = image.width;
    canvas.height = image.height;

    if (ctx) {
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      ctx.drawImage(image, 0, 0, ctx.canvas.width, ctx.canvas.height);
    }
  };

  const onImageChange = async ({ target }) => {
    const canvas = canvasRef.current;
    if (canvas) {
      const ctx = (canvas as HTMLCanvasElement).getContext("2d");
      drawImageOnCanvas(target, canvas, ctx);

      const tensor = tf.browser.fromPixels(canvas); // Convert the image data to a tensor
      const resized = tf.image.resizeBilinear(tensor, [64, 64]).toFloat();
      const batched = resized.expandDims(0); // Add a dimension to get a batch shape

      const prediction = await model!.predict(batched).dataSync();
      console.log("Prediction: " + prediction[0]);
      setPrediction(prediction[0]);
    }
  };

  const interpretingPrediction = () => {
    let interpretation = "";
    if (prediction === 0) {
      interpretation = "Sorry, but you're not so beautiful...";
    } else if (prediction === 1) {
      interpretation = "Wow! You're hot!";
    }
    return interpretation;
  };

  const removePicture = () => {
    setImageSrc("");
  };

  useEffect(() => {
    const loadModel = async () => {
      try {
        const model = await tf.loadLayersModel(modelUrl);
        console.log("Successfully loaded model ");
        setModel(model);
      } catch (err) {
        console.log(err);
      }
    };
    loadModel();
  }, []);

  return (
    <PageLayout>
      <Section>
        <Stack spacing="6" mb="16">
          <H1>
            How{" "}
            <H1 as="span" bgGradient="linear(to-l, #7928CA, #FF0080)" bgClip="text">
              hot
            </H1>{" "}
            are you ?
          </H1>
          <Text color={mode(`gray.500`, `whiteAlpha.800`)}>
            Let our Artificial Intelligence decide how attractive you are. Do you want to know as how others perceived
            you? Find it out now by uploading a photo of your face.
          </Text>
        </Stack>

        {model ? (
          !imageSrc ? (
            <ImageUploader onUpload={onBeginUpload} />
          ) : (
            <>
              <canvas className="classified-image" ref={canvasRef}>
                {imageSrc && <Img alt="preview" onLoad={onImageChange} src={imageSrc} />}
              </canvas>
              <Text>{interpretingPrediction()}</Text>
              <Button onClick={() => removePicture()}>Try with another picture</Button>
            </>
          )
        ) : (
          <LoadModel />
        )}
      </Section>
    </PageLayout>
  );
};
export default Home;
